const request = require('request');
const MPoster = require('./MPoster');


MPoster.prototype.postProduct = function (session, token, product) {

  const categoryMap = {
    "All terrain": 199,
    "All terrain extreme gtx": 186,
    "CL Leather sm": 187,
    "Classic": 152,
    "Classic DL 6000 wr": 196,
    "Classic Leather suede": 188,
    "Classic multi": 184,
    "Classic royal": 171,
    "Classic Winter": 151,
    "Cordura": 190,
    "CrossFit Nano": 176,
    "Easytone": 173,
    "Easytone зимние": 172,
    "Elite Stride": 193,
    "Gore tex": 174,
    "Jogger": 191,
    "Leather": 181,
    "Leather bleck gum": 183,
    "Memory tech": 177,
    "One cushion ": 200,
    "Original": 178,
    "Ortholite": 182,
    "Princess": 197,
    "Stormshell": 179,
    "Thinsulate": 180,
    "Zigtech": 153,
    "Zigtrail mobilize ii": 185,
    "DMX esde adventure": 192,
    "мужские": 161,
    "женские": 162
  };

  const sizeMapFemale = {
    '36': '90',
    '37': '91',
    '38': '92',
    '39': '93',
    '40': '94'
  };
  const sizeMapMale = {
    '41': '95',
    '42': '96',
    '43': '97',
    '44': '98',
    '45': '99',
    '46': '107'
  };
  const sizeMap = {
    '36': '90',
    '37': '91',
    '38': '92',
    '39': '93',
    '40': '94',
    '41': '95',
    '42': '96',
    '43': '97',
    '44': '98',
    '45': '99',
    '46': '107'
  };

  let sku = '';

  let formData = {
    'product_description[1][name]': product.title,
    'product_description[1][seo_h1]': product.title,
    'product_description[1][seo_title]': product.title,
    'product_description[1][meta_keyword]': product.title,
    'product_description[1][meta_description]': product.title,
    'product_description[1][description]': product.description
      .map(item => '<p>' + item + '</p>')
      .join(''),
    'keyword': product.seoTitle,
    'model': product.sku,
    'price': product.price ? Math.round(product.price + 
      product.price * (20 + Math.random() * 10) / 100) : 0,
    'stock_status_id': '7',
    'tax_class_id': '0',
    'subtract': '0',
    'quantity': '1000',
    'date_available': (new Date()).toISOString().substring(0, 10),
    'length_class_id': '1',
    'weight_class_id': '1',
    'status': '1',
    'onmain': '1',
    'sort_order': '1',
    'image': 'data/' + product.pics[0],
    'main_category_id': '59',
    'product_store[]': '0',
    'product_category[]': ['59'],

    'product_special[0]': '1',
    'product_special[0][customer_group_id]': '1',
    'product_special[0][priority]': '0',
    'product_special[0][price]': product.price ? product.price : 0,
    'product_special[0][date_start]': '2018-01-01',
    'product_special[0][date_end]': '2019-01-01',

    'product_option[0][product_option_id]': '397',
    'product_option[0][name]': 'Размер',
    'product_option[0][option_id]': '15',
    'product_option[0][type]': 'select'
  };

  product.pics.slice(1).forEach((pic, index) => 
    formData['product_image[' + index + '][image]'] = 'data/' + pic);

  let mapCategory = (data, mapping) => {
    for (let key in mapping) {
      if (mapping.hasOwnProperty(key)) {
        let regexp = new RegExp(key, 'i');
        if (product.title.search(regexp) !== -1) {
          data.push(mapping[key]);
        }
      }
    }
    return data;
  }

  let addOptions = (data, options) => {
    for (let key in options) {
      if (options.hasOwnProperty(key)) {
        data['product_option[0][product_option_value][' + i + '][option_value_id]'] = options[key];
        data['product_option[0][product_option_value][' + i + '][quantity]'] = '1000';
        i++;
      }
    }
    return data;
  }

  // Category
  formData['product_category[]'] = mapCategory(formData['product_category[]'], categoryMap);

  // Sizes
  let i = 0;
  if (product.title.search(/мужские/i) !== -1) {
    formData = addOptions(formData, sizeMapMale);
  } else if (product.title.search(/женские/i) !== -1) {
    formData = addOptions(formData, sizeMapFemale);
  } else {
    formData = addOptions(formData, sizeMap);
  }

  // Price
  if (product.price < 4000) {
    formData['product_category[]'].push('202');
  } else if (product.price >= 4000 && product.price < 7000) {
    formData['product_category[]'].push('203');
  } else if (product.price >= 7000) {
    formData['product_category[]'].push('204');
  }
  console.log(this.products.length, product.title);

  const url = [
    this.url,
    '/admin/index.php?route=catalog/product/insert',
    '&token=',
    token].join('');

  request.post({
    url: url,
    headers: { 
      'Cookie': session + ' currency=RUB',
      'authorization': 'Basic bWFuYWdlcjpvcGVuaG9zdHBsejE='
    },
    formData: formData
  }, (err, httpResponse, body) => {
    if (err) {
      return console.error(err);
    }
    if (this.products.length) {
      setTimeout(() => this.postProduct(session, token, this.products.shift()), 10000);
    }
  });
};

module.exports = MPoster;