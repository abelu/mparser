const indexNav = `
<ul id="nav" class="grid-full"> 
  <li class="level nav-1 first  no-level-thumbnail ">
    <a style="background-color:" href="https://dfstore.com/ru/jumpsuits">
      <div class="thumbnail"></div>
      <span style="color:;  ">Комбинезоны</span>
    </a>
  </li>
</ul>
`;

const categoryPages = `
<div class="toolbar-bottom">
  <div class="pages">
    <strong>Страница:</strong>
    <ol>
      <li class="current">1</li>
      <li><a href="https://dfstore.com/ru/t-shirts?limit=12&amp;p=2">2</a></li>
      <li><a href="https://dfstore.com/ru/t-shirts?limit=12&amp;p=3">3</a></li>
      <li>
        <a class="next i-next fa fa-caret-right" href="https://dfstore.com/ru/t-shirts?limit=12&amp;p=2" title="Следующая">
          <!-- <img src="https://dfstore.com/skin/frontend/default/theme699/images/pager_arrow_right.gif" alt="Следующая" class="v-middle" /> -->
        </a>
      </li>
    </ol>
  </div>
</div>
`;

const categoryPage = `
<ul class="products-grid row first odd">
  <li class="item first col-xs-12 col-sm-3" itemscope="" itemtype="http://schema.org/product">
    <div class="wrapper-hover">
      <a href="https://dfstore.com/ru/shut-up" title="Спортивная майка-распашонка Shut Up " class="product-image" itemprop="url">
        <img src="https://dfstore.com/media/catalog/product/cache/7/small_image/210x315/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b0699.jpg" alt="Спортивная майка-распашонка Shut Up ">
      </a>
    </div>
  </li>
</ul>
`;

const productPage = `
<div class="product-shop">
  <div class="product-name name-wrapper-product-1535">
    <h1 itemprop="name">Спортивная майка PRO Black</h1>
  </div>
  <div class="row-product">
    <p class="no-rating">
      <a class="rating-grouped" href="https://dfstore.com/ru/review/product/list/id/1535/category/96/#review-form">Разместите первым отзыв для этого товара</a>
    </p>
  </div>
                        
  <a href="javascript:void(0)" class="ajax-size-chart">Размерная сетка</a>
  <div class="product-sku-price-wrapper">       
    <div class="price-box" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
      <span class="regular-price" itemprop="price" id="product-price-1535">
        <span class="price">2&nbsp;535&nbsp; руб</span>
      </span>                    
    </div>

    <p class="sku-block product-1535">Артикул: 813400101 </p>              
  </div>
          
  <div class="clear"></div>

  <div class="product-options-bottom">            
    <div class="price-box" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
      <span class="regular-price" itemprop="price" id="product-price-1535_clone">
        <span class="price">2&nbsp;535&nbsp; руб</span>
      </span>
    </div>
  </div>
</div>
<ul data-productid="1535">
  <li data-lightbox="https://dfstore.com/media/catalog/product/cache/7/image/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3545.jpg">
    <div class="hover-image" style="display: inline-block;">
      <img src="https://dfstore.com/media/catalog/product/cache/7/image/67x100/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3545.jpg" alt="Спортивная майка PRO Black" data-lightbox="https://dfstore.com/media/catalog/product/cache/7/image/323x485/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3545.jpg">
    </div>
  </li>
  <li data-lightbox="https://dfstore.com/media/catalog/product/cache/7/image/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3549.jpg">
    <div class="hover-image" style="display: inline-block;">
        <img src="https://dfstore.com/media/catalog/product/cache/7/image/67x100/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3549.jpg" alt="Спортивная майка PRO Black" data-lightbox="https://dfstore.com/media/catalog/product/cache/7/image/323x485/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3549.jpg">
    </div>
  </li>
</ul>
<div class="box-description">
  <h2>Подробности</h2>
  <div class="box-description product-1535">
    <p>Спортивная майка PRO Black для настоящих ценителей спорта</p>
    <p>Ткань с компрессионным эффектом обеспечивает максимальную поддержку во время интенсивных нагрузок.</p>
    <p>Идеально подходит для занятий в тренажерном зале, велопрогулок, пробежек на свежем воздухе и активных прогулок.</p>
    <p>&nbsp;</p>
    <p>- Гипоаллергенная ткань;</p>
    <p>- Быстро и эффективно отводит влагу;</p>
    <p>- Не влияет на функции теплообмена, позволяет коже дышать;</p>
    <p>- Облегающий крой не ощущается на теле;</p>
    <p>- Состав: 80% полиэстер, 20% Lycra Sport;</p>
    <p>- Машинная стирка.</p>                        
  </div>
</div>
`;

const MParser = require('../../MParser');

describe("MParser", function() {
  let parser;

  beforeEach(() => {
    parser = new MParser();
  });

  it('should create MParser object', () => {
    expect(parser).toBeTruthy();
    expect(parser.crawler).toBeTruthy();
    expect(parser.fs).toBeTruthy();
  });

  it('should get categories list', done => {
    let end = done;
    let fakeCallback = (error, res, done) => {
      //indexCallback.call(parser, [error, res, done]);
      let list = parser.parseIndex(res);
      expect(list).toBeTruthy();
      expect(list.length).toBe(1);
      expect(list[0].url).toBe('https://dfstore.com/ru/jumpsuits?limit=36');
      expect(list[0].title).toBe('Комбинезоны');
      done();
      end();
    };
    parser.crawler.queue([{
      html: indexNav,
      callback: fakeCallback
    }])
  });

  it('should get category pages', done => {
    let end = done;
    let fakeCallback = (error, res, done) => {
      let list = parser.parseCategory(res);
      expect(list).toBeTruthy();
      expect(list.length).toBe(2);
      expect(list[0].url).toBe('https://dfstore.com/ru/t-shirts?limit=12&p=2');
      expect(list[1].url).toBe('https://dfstore.com/ru/t-shirts?limit=12&p=3');
      done();
      end();
    };
    parser.crawler.queue([{
      html: categoryPages,
      callback: fakeCallback
    }]);
  });

  it('should get category pages w\o pagination', done => {
    let end = done;
    let fakePage = (error, res, done) => {
      let list = parser.parseCategory(res);
      expect(list).toBeTruthy();
      expect(list.length).toBe(0);
      done();
      end();
    };
    parser.crawler.queue([{
      html: indexNav,
      callback: fakePage
    }]);
  });

  it('should get products from category page', done => {
    let end = done;
    let fakeCallback = (error, res, done) => {
      let list = parser.parseCategoryPage(res);
      expect(list).toBeTruthy();
      expect(list.length).toBe(1);
      expect(list[0].url).toBe('https://dfstore.com/ru/shut-up');
      expect(list[0].title).toBe('Спортивная майка-распашонка Shut Up ');
      done();
      end();
    };
    parser.crawler.queue([{
      html: categoryPage,
      callback: fakeCallback
    }]);
  });

  it('should get product gata form product page', done => {
    let end = done;
    let fakeCallback = (error, res, done) => {
      let product = parser.parseProduct(res);
      expect(product).toBeTruthy();
      expect(product.title).toBe('Спортивная майка PRO Black');
      expect(product.price).toBe(2535);
      expect(product.sku).toBe('813400101');

      expect(product.images).toBeTruthy();
      expect(product.images.length).toBe(2);
      expect(product.images[0]).toBe('https://dfstore.com/media/catalog/product/cache/7/image/c96a280f94e22e3ee3823dd0a1a87606/c/z/cz6b3545.jpg');

      expect(product.description).toBeTruthy();
      expect(product.description.length).toBe(10);
      expect(product.description[9]).toBe('- Машинная стирка.');

      end();
    };
    parser.crawler.queue([{
      html: productPage,
      callback: fakeCallback
    }]);
  });
});