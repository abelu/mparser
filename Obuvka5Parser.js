const MParser = require('./MParser');

MParser.prototype.parseIndex = function (res) {
  return res.$('.left-menu>ul>li>a').map((index, el) => {
    return {
      url: res.$(el).attr('href').replace(' ', ''),
      title: res.$(el).text()
    }
  }).get();
};

MParser.prototype.parseCategory = function (res) {
  return [];
};

MParser.prototype.parseCategoryPage = function (res) {
  return res.$('.item-image>a').map((index, el) => {
    return {
      url: res.$(el).attr('href'),
      title: res.$(el).children('img').attr('alt')
    }
  }).get();
};

MParser.prototype.parseProduct = function (res) {
  let $ = res.$;
  try {
    return {
      title: res.options.title,
      price: parseInt(
        ($('.price-new').length ? $('.price-new') : $('.price'))
        .html()
        .replace('.', '')
        .replace(/&#xA0;/g, '')
        .replace(/&#x440;&#x443;&#x431;/, '')),
      sku: res.options.title,
      images: $('a.small_foto_tovar')
        .map((index, el) => $(el).attr('href'))
        .get(),
      description: $('#tab-description>*')
        .map((index, el) => $(el).text().replace(/[\r\n\t]/g, ''))
        .get()
    }
  } catch (e) {
    console.error(e);
    return null;
  }
}

module.exports = MParser;