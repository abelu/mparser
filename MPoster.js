const fs = require('fs');
const request = require('request');
let poster;

function MPoster() {
  this.fs = fs;
  this.request = request;
  poster = this;
}

MPoster.prototype.postProduct = (session, token, product) => {

  const categoryMap = {
    'jumpsuits': 92,
    'sets': 64,
    'leggins': 62,
    'leggings': 62,
    'tops': 61,
    'shorts': 60,
    'accessories': 78,
    'men': 158,
    't-shirts': 80
  };

  const sizeMap = {
    'XS': '82',
    'S': '76',
    'M': '77',
    'L': '78',
    'XL': '79'/*,
    'XXL': 80,
    'XXXL': 81*/
  };

  const colors = {
    'Blue': 191,
    'Lemon': 163,
    'Pink': 164,
    'Grey': 165,
    'Frulatto': 166,
    'Black': 167,
    'Milkshake': 170,
    'Red': 171,
    'Green': 172,
    'Orange': 173,
    'Crystal': 174,
    'Violet': 175,
    'Lavender': 176,
    'Confetto': 177
  }

  const prints = {
    'Cruise': 169,
    'Como': 169,
    'Rainbow': 169,
    'Yummy': 169,
    'Cocktails': 169,
    'Milkshake': 169,
    'Vintage': 169,
    'Tattoo': 169,
    'Comics': 169,
    'Techno': 169,
    'Diamonds': 169,
    'Abstract': 169,
    'Geometric': 169,
    'Hero': 169,
    'Meal': 169,
    'Toxic': 169,
    'Space': 169,
    'Disco': 169,
    'Sky': 169,
    'Tigers': 169,
    'Silver': 169
  }

  const formData = {
    'product_description[1][name]': product.title,
    'product_description[1][seo_h1]': product.title,
    'product_description[1][seo_title]': product.title,
    'product_description[1][meta_keyword]': product.title,
    'product_description[1][meta_description]': product.title,
    'product_description[1][description]': product.description
      .map(item => '<p>' + item + '</p>')
      .join(''),
    'keyword': product.seoTitle,
    'model': product.sku,
    'price': product.price ? Math.round(product.price + 
      product.price * (20 + Math.random() * 10) / 100) : 0,
    'stock_status_id': '7',
    'tax_class_id': '0',
    'subtract': '0',
    'quantity': '1000',
    'date_available': (new Date()).toISOString().substring(0, 10),
    'length_class_id': '1',
    'weight_class_id': '1',
    'status': '1',
    'onmain': '1',
    'sort_order': '1',
    'image': 'data/' + product.pics[0],
    'main_category_id': categoryMap[product.parent],
    'product_store[]': '0',
    'product_category[]': ['59'],

    'product_special[0]': '1',
    'product_special[0][customer_group_id]': '1',
    'product_special[0][priority]': '0',
    'product_special[0][price]': product.price ? product.price : 0,
    'product_special[0][date_start]': '2018-01-01',
    'product_special[0][date_end]': '2019-01-01',

    'product_option[0][product_option_id]': '397',
    'product_option[0][name]': 'Размер',
    'product_option[0][option_id]': '15',
    'product_option[0][type]': 'select'
  };

  product.pics.slice(1).forEach((pic, index) => 
    formData['product_image[' + index + '][image]'] = 'data/' + pic);

  let mapCategory = (data, mapping) => {
    for (let key in mapping) {
      if (mapping.hasOwnProperty(key)) {
        let regexp = new RegExp(key, 'i')
        if (product.title.search(regexp) !== -1) {
          data.push(mapping[key]);
        }
      }
    }
    return data;
  }

  // Sizes
  let i = 0;
  for (let key in sizeMap) {
    if (sizeMap.hasOwnProperty(key)) {
      formData['product_option[0][product_option_value][' + i + '][option_value_id]'] = sizeMap[key];
      formData['product_option[0][product_option_value][' + i + '][quantity]'] = '1000';
      i++;
    }
  }

  // Colors
  formData['product_category[]'] = mapCategory(formData['product_category[]'], colors);

  // Prints
  formData['product_category[]'] = mapCategory(formData['product_category[]'], prints);

  // Price
  if (product.price < 2000) {
    formData['product_category[]'].push('186');;
  } else if (product.price >= 2000 && product.price < 4000) {
    formData['product_category[]'].push('187');
  } else if (product.price >= 4000 && product.price < 6000) {
    formData['product_category[]'].push('188');
  } else if (product.price >= 6000) {
    formData['product_category[]'].push('189');
  }

  // Sex
  if (product.parent === 'men') {
    formData['product_category[]'].push('184');
  } else {
    formData['product_category[]'].push('183');
  }

  // Length
  if (product.parent === 'leggings' || product.parent === 'leggins') {
    formData['product_category[]'].push('179');
  }
  if (product.parent === 'shorts') {
    formData['product_category[]'].push('181');
  }

  console.log(poster.products.length, product.title);

  const url = [
    poster.url,
    '/admin/index.php?route=catalog/product/insert',
    '&token=',
    token].join('');

  request.post({
    url: url,
    headers: { 'Cookie': session + ' currency=RUB' },
    formData: formData
  }, (err, httpResponse, body) => {
    if (err) {
      return console.error(err);
    }
    if (poster.products.length) {
      setTimeout(() => poster.postProduct(session, token, poster.products.shift()), 10000);
    }
  });
}

MPoster.prototype.auth = data => {
  const formData = {
    username: data.username,
    password: data.password
  };

  request.post({ 
    url: poster.url + '/admin/',
    headers: {
      'authorization': 'Basic bWFuYWdlcjpvcGVuaG9zdHBsejE='
    },
    formData: formData 
  }, (err, httpResponse, body) => {
      if (err) {
        return console.error('error: ' + err);
      }
      if (httpResponse.headers.status !== '302' ||
        !httpResponse.headers.location) {
        return console.error('invalid response: ' + httpResponse.headers.status);
      }
      let token = httpResponse.headers.location.split('&token=').pop();
      if (!token) {
        return console.error('invalid token: ' + token);
      }
      let cookies = httpResponse.headers['set-cookie'];
      let session = cookies.join(' ').match(/PHPSESSID=[^;]+;/g)[0];
      if (!session) {
        return console.error('invalid session: ' + session);
      }
      console.log(session);
      console.log('token=' + token);
      poster.postProduct(session, token, poster.products.shift());
    }
  );
}

MPoster.prototype.run = options => {
  poster.url = options.url;
  poster.input = options.input
  poster.username = options.username;
  poster.password = options.password;

  fs.readFile('./output/' + poster.input, (err, data) => {
    if (err) throw err;
    poster.products = JSON.parse(data);

    poster.auth({
      username: poster.username,
      password: poster.password
    });
  });
}

module.exports = MPoster;