const fs = require('fs');

fs.readFile('./output/output.json', (err, data) => {
  if (err) throw err;
  const products = JSON.parse(data);
  const result = {};
  products.forEach(item => {
    item.title.split(' ').forEach(word => {
      if (!result[word]) {
        result[word] = 1;
      }
      result[word]++;
    })
  });
  console.log(result);
});