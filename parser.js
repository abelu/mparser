//let MParser = require('./MParser');
let Obuvka5Parser = require('./Obuvka5Parser');

/*let parser = new MParser();
parser.run('http://dfstore.com/ru/', [ 
  'https://dfstore.com/ru/new',
  'https://dfstore.com/ru/lookbook'/*,
  'https://dfstore.com/ru/men',
  'https://dfstore.com/ru/accessories',
  'https://dfstore.com/ru/t-shirts',
  'https://dfstore.com/ru/shorts',
  'https://dfstore.com/ru/tops',
  'https://dfstore.com/ru/leggings',
  'https://dfstore.com/ru/jumpsuits',
  'https://dfstore.com/ru/sets',
], 'dfstore', false);*/

let parser = new Obuvka5Parser();
parser.run('https://obuvka5.ru/', [
  'https://obuvka5.ru/adidas/',
  'https://obuvka5.ru/asics/',
  'https://obuvka5.ru/converse/',
//  'https://obuvka5.ru/reebok/',
  'https://obuvka5.ru/vans/',
  'https://obuvka5.ru/timberland/',
  'https://obuvka5.ru/isabel_marant/',
  'https://obuvka5.ru/nike/',
  'https://obuvka5.ru/newbalance/'
], 'obuvka5', false);