const Crawler = require('crawler');
const cyrillicToTranslit = require('cyrillicToTranslit');
const fs = require('fs');
let parser;

function MParser(param) {
  this.crawler = new Crawler({
    maxConnections : 10,
    callback: function (error, res, done) {
      if (error) {
        console.log(error);
      }
      done();
    }
  });
  this.fs = fs;
  this.exclude = [];
  parser = this;
};

MParser.prototype.parseIndex = function (res) {
  return res.$('#nav a').map((index, el) => {
    return {
      url: res.$(el).attr('href').replace(' ', '') + '?limit=36&p=1',
      title: res.$(el).children('span').text()
    }
  }).get();
};

MParser.prototype.parseCategory = function (res) {
  return res.$('.toolbar-bottom .pages>ol>li>a:not(.next):not(.current)').map((index, el) => {
    return res.$(el).attr('href').replace(/&amp;/g, '&');
  }).get();
};

MParser.prototype.parseCategoryPage = function (res) {
  return res.$('.products-grid .wrapper-hover>a.product-image').map((index, el) => {
    return {
      url: res.$(el).attr('href'),
      title: res.$(el).attr('title')
    }
  }).get();
};

MParser.prototype.parseProduct = function (res) {
  let $ = res.$;
  let priceEl = $('.price');
  let price = 0;
  if ($('.sku-block').length !== 1) {
    priceEl.each((index, el) => {
      let element = $(el);
      if (element.html()) {
        price += parseInt(element
          .html()
          .replace(/&#xA0;/g, '')
          .replace(/&#x440;&#x443;&#x431;/, ''));
      }
    })
  } else {
    price = parseInt(priceEl
      .html()
      .replace(/&#xA0;/g, '')
      .replace(/&#x440;&#x443;&#x431;/, ''));
  }
  let sku = $('.product-shop .sku-block').eq(0)
    .text()
    .replace(/Артикул: /, '')
    .replace(' ', '');
  return {
    title: $('.product-shop>.product-name>h1').eq(0).text(),
    price: price,
    sku: sku ? sku : Math.floor(Math.random() * 9999999999),
    images: $('li[data-lightbox]')
      .map((index, el) => $(el).attr('data-lightbox'))
      .get(),
    description: $('.box-description>p')
      .map((index, el) => $(el).text())
      .get()
  }
}

MParser.prototype.indexCallback = function (error, res, done) {
  if (error) {
    console.log(error);
  } else {
    let list = parser.parseIndex(res);
    parser.crawler.queue(list.map((item, index) => {
      return {
        url: item.url,
        title: item.title,
        callback: parser.categoryCallback
      }
    }))
  }
  done();
};

MParser.prototype.categoryCallback = function (error, res, done) {
  if (error) {
    console.log(error);
  } else {
    if (parser.exclude.indexOf(res.options.url.split('?').shift()) !== -1) {
      return done();
    }
    console.log(res.options.url);
    let alias = decodeURIComponent(res.options.title);
    let dirPath = './output/' + parser.outputPath + '/' + alias; 
    try {
      parser.fs.mkdirSync(dirPath)
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err;
      }
    }
    let list = parser.parseCategory(res);
    list.push(res.options.url);
    parser.crawler.queue(list.map((item, index) => {
      return {
        url: item,
        callback: parser.categoryPageCallback,
        parent: alias
      }
    }))
  }
  done();
};

MParser.prototype.categoryPageCallback = function (error, res, done) {
  if (error) {
    console.log(error);
  } else {
    console.log(res.options.url);
    let list = parser.parseCategoryPage(res);

    parser.crawler.queue(list.map((item, index) => {
      return {
        url: item.url,
        title: item.title,
        callback: parser.productCallback,
        alias: res.options.parent
      }
    }))
  }
  done();
};

MParser.prototype.productCallback = function (error, res, done) {
  if (error) {
    console.log(error);
  } else {
    console.log(res.options.url);
    let filename = cyrillicToTranslit().translit(res.options.title, '_');
    console.log(filename);
    let product = parser.parseProduct(res);
    if (!product) {
      return done();
    }

    let dirPath = ['./output/' + parser.outputPath + '/', res.options.alias, '/', filename].join('');
    try {
      parser.fs.mkdirSync(dirPath)
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err;
      }
    }

    product.url = res.options.url;
    product.seoTitle = filename;
    product.parent = res.options.alias;
    product.pics = product.images.map((item, index) => 
      [dirPath.replace(/\.\/[^\/]+\//, ''), '/', filename, '-', (index + 1), '.jpg'].join(''));
    parser.output.push(product);

    if (parser.pullImages) {
      parser.crawler.queue(product.images.map((item, index) => {
        return {
          url: item,
          jQuery: false,
          encoding: null,
          folder: dirPath,
          filename: filename + '-' + (index + 1) + '.jpg',
          callback: parser.imageCallback
        }
      }))
    }
  }
  done();
};

MParser.prototype.imageCallback = function (error, res, done) {
  if (error) {
    console.log(error);
  } else {
    let filename = res.options.folder + '/' + res.options.filename;
    console.log(filename);
    try {
      if (!parser.fs.existsSync(filename)) {
        parser.fs.createWriteStream(filename).write(res.body);
      }
    } catch (e) {
      console.log(e);
    }
  }
  done();
};

MParser.prototype.run = function (url, exclude, outputPath, pullImages) {
  this.outputPath = outputPath;
  this.output = [];
  this.exclude = exclude;
  this.pullImages = pullImages;
  let filename = './output/' + outputPath + '/' + outputPath + '.json'
  this.crawler.on('drain', () => {
    if (this.fs.existsSync(filename)) {
      this.fs.unlink(filename)
    }
    this.fs.createWriteStream(filename).write(
      JSON.stringify(this.output));
  });
  this.crawler.queue([{
    url: url,
    callback: this.indexCallback
  }]);
};

module.exports = MParser;